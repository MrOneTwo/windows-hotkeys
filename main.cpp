#include <Windows.h>
#include <stdio.h>
#include <tchar.h>
#include <wchar.h>

#include "keycodes.h"
#include "resource.h"

typedef bool bool32;

static int WINDOW_WIDTH = 1200;
static int WINDOW_HEIGHT = 700;

static HWND hwndMain = {0};
static HWND targetWindowHandle;
static HWND hwndLog;
static HWND hwndEditFilename;
static HWND hwndEditPath;
static HWND hwndSaveAs;

#define BUFFER_LOG_SIZE   1024
static wchar_t bufferWLog[BUFFER_LOG_SIZE];
static wchar_t bufferWEditFilename[BUFFER_LOG_SIZE];
static wchar_t bufferWEditPath[BUFFER_LOG_SIZE];
static char bufferConsole[512];

static wchar_t bufferWScratch[512];

static bool32 timerToggle = false;
static UINT_PTR timer1;
static bool32 timer1Active = false;

typedef struct _WindowsList {
    HWND wl[512];
    HWND* cursor;
    int size;
    int count;
} WindowsList;

WindowsList windowsListA;

void addToWindowsList(WindowsList* list, HWND h) {
    *list->cursor = h;
    list->cursor++;
    // TODO(michalc): is it ok to use void*? Will arithmetic be ok?
    if (list->count >= list->size)
    {
        list->cursor = list->wl;
        list->count = 512;
    }
    list->count++;
}

void initWindowsList(WindowsList* list) {
    list->size = 512;
    list->cursor = list->wl;
    list->count = 0;
}

WindowsList filterWindowsListByClass(WindowsList* list, char* s) {
    WindowsList _list = {};
    initWindowsList(&_list);
    char buffer[256];
    for (int i = 0; i < list->count; ++i)
    {
        GetClassName(list->wl[i], buffer, 256);
        if (strstr(buffer, s) != NULL)
        {
            addToWindowsList(&_list, list->wl[i]);
        }
    }
    return _list;
}

WindowsList filterWindowsListByText(WindowsList* list, char* s) {
    WindowsList _list = {};
    initWindowsList(&_list);
    char buffer[256];
    for (int i = 0; i < list->count; ++i)
    {
        GetWindowTextA(list->wl[i], buffer, 256);
        if (strstr(buffer, s) != NULL)
        {
            addToWindowsList(&_list, list->wl[i]);
        }
    }
    return _list;
}

BOOL CALLBACK EnumWindowsProc(HWND hwndWin, LPARAM lParam)
{
    char buffer[256];
    GetWindowTextA(hwndWin, buffer, 256);
    sprintf(bufferConsole, "Windows handle: %p %s\n", hwndWin, buffer);
    WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), bufferConsole, strlen(bufferConsole), 0, NULL);
    addToWindowsList(&windowsListA, hwndWin);
    return true;
}

void PushToLogBuffer(wchar_t* buf, wchar_t* content)
{
    wchar_t _buf[64] = {};
    wcsncpy(_buf, content, wcslen(content));
    wmemmove(buf + wcslen(_buf), buf, BUFFER_LOG_SIZE - wcslen(_buf));
    wcsncpy(buf, _buf, wcslen(_buf));
    SetWindowTextW(hwndLog, buf);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
        case WM_HOTKEY:
            switch(LOWORD(lParam))
            {
                case MOD_CONTROL:
                    sprintf(bufferConsole, "WM_HOTKEY message received... (%x)\n", HIWORD(lParam));
                    WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), bufferConsole, strlen(bufferConsole), 0, NULL);
                    break;
                case MOD_SHIFT:
                    break;
                case MOD_ALT:
                    break;
                default: {
                    switch(HIWORD(lParam))
                    {
                        case KEY_Q:
                            PostQuitMessage(0);
                            break;
                        case KEY_F:
                            PushToLogBuffer(bufferWLog, L" [WM_HOTKEY] Pressed 'f'\r\n");
                            hwndSaveAs = FindWindowW(NULL, L"Save As");
                            if (hwndSaveAs != 0)
                            {
                                PushToLogBuffer(bufferWLog, L" Found window: Save As\r\n");

                                initWindowsList(&windowsListA);
                                EnumChildWindows(hwndSaveAs, EnumWindowsProc, NULL);
                                windowsListA = filterWindowsListByClass(&windowsListA, "Edit");
                                wsprintfW(bufferWScratch, L"  - filtered windows count (filter: class - 'Edit'): %d \r\n", windowsListA.count);
                                PushToLogBuffer(bufferWLog, bufferWScratch);

                                char buffer[256];
                                GetWindowTextA(hwndEditFilename, buffer, 256);
                                // TODO(michalc): using index is a very bad idea.
                                SendMessageA(windowsListA.wl[0], WM_SETTEXT, NULL, (LPARAM)buffer);

                                initWindowsList(&windowsListA);
                                EnumChildWindows(hwndSaveAs, EnumWindowsProc, NULL);
                                windowsListA = filterWindowsListByText(&windowsListA, "Address:");
                                wsprintfW(bufferWScratch, L"  - filtered windows count (filter: text - 'Address'): %d \r\n", windowsListA.count);
                                PushToLogBuffer(bufferWLog, bufferWScratch);

                                SendMessage(windowsListA.wl[0], WM_SETFOCUS, 0, 0);
                                SendMessage(windowsListA.wl[0], WM_KEYDOWN, (WPARAM)VK_RETURN, 0);
                                SendMessage(windowsListA.wl[0], WM_KEYUP, (WPARAM)VK_RETURN, 0);
                                SendMessage(windowsListA.wl[0], WM_SETTEXT, NULL, (LPARAM)"Address C:\\");
                            }
                            break;
                        case KEY_L:
                            EnumWindows(EnumWindowsProc, NULL);
                            PushToLogBuffer(bufferWLog, L" [WM_HOTKEY] Pressed 'l'\r\n");
                            //SetForegroundWindow(targetWindowHandle);
                            break;
                        case KEY_R:
                            PushToLogBuffer(bufferWLog, L" [WM_HOTKEY] Pressed 'r'\r\n");
                            break;
                        case KEY_F4:
                            PushToLogBuffer(bufferWLog, L"Activating hotkeys\r\n");
                            // Since HWND is NULL then 1001 is ignored and a Timer with new ID is created.
                            if (timer1Active == false)
                            {
                                timer1 = SetTimer(hwndMain, 1001, 2*1000, NULL);  // in miliseconds
                                timer1Active = true;
                                RegisterHotKey(hwndMain, 1, MOD_NOREPEAT, KEY_Q);
                                RegisterHotKey(hwndMain, 2, MOD_NOREPEAT, KEY_L);
                                RegisterHotKey(hwndMain, 3, MOD_NOREPEAT, KEY_R);
                                RegisterHotKey(hwndMain, 4, MOD_NOREPEAT, KEY_F);
                                RegisterHotKey(hwndMain, 5, MOD_NOREPEAT, KEY_T);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                }
            }
        case WM_MOVE:
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_QUIT:
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
        case WM_TIMER:
            PushToLogBuffer(bufferWLog, L"Deactivating hotkeys\r\n");
            UnregisterHotKey(hwndMain, 1);
            UnregisterHotKey(hwndMain, 2);
            UnregisterHotKey(hwndMain, 3);
            UnregisterHotKey(hwndMain, 4);
            UnregisterHotKey(hwndMain, 5);
            KillTimer(hwndMain, timer1);
            timer1Active = false;
            break;
        case WM_SIZE:
            WINDOW_WIDTH = LOWORD(lParam);
            WINDOW_HEIGHT = HIWORD(lParam);
            SetWindowPos(hwndLog, HWND_TOP, 10, 10, WINDOW_WIDTH - 2*10, WINDOW_HEIGHT - GetSystemMetrics(SM_CYCAPTION) - 2*10, SWP_NOZORDER);
            PushToLogBuffer(bufferWLog, L"");
            break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

int WINAPI WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow)
{           
    initWindowsList(&windowsListA);

    AllocConsole();

    WNDCLASSEX wc;
    MSG msg = {0};
    const char g_szClassName[] = "myWindowClass";
    RECT windowRect = {0};

    //Step 1: Registering the Window Class
    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = 0;
    wc.lpfnWndProc   = WndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = g_szClassName;
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if(!RegisterClassEx(&wc))
    {
        sprintf(bufferConsole, "Failed to register window class...\n");
        WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), bufferConsole, strlen(bufferConsole), 0, NULL);
        return 0;
    }

    // Step 2: Creating the Window
    hwndMain = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        g_szClassName,
        "The title of my window",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, WINDOW_WIDTH, WINDOW_HEIGHT,
        NULL, NULL, hInstance, NULL);

    if(hwndMain == NULL)
    {
        sprintf(bufferConsole, "Failed to create the main window...\n");
        WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), bufferConsole, strlen(bufferConsole), 0, NULL);
        return 0;
    }

    ShowWindow(hwndMain, nCmdShow);
    UpdateWindow(hwndMain);

    //HICON hMyIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYICON));
    wcscpy(bufferWEditPath, L"C:\0");
    wcscpy(bufferWEditFilename, L"file.txt\0");

    // TODO(michalc): the subwindow size is wrong.
    #define LINE_SIZE 20
    #define PADDING 10
    int winH = LINE_SIZE;
    hwndEditPath = CreateWindowW(L"edit", reinterpret_cast<LPCWSTR>(bufferWEditPath), WS_CHILD | WS_VISIBLE | SS_LEFT | WS_BORDER,
                                  PADDING, PADDING, WINDOW_WIDTH - 2*10 - WINDOW_WIDTH/2, winH,
                                  hwndMain, (HMENU)1, (HINSTANCE)NULL, (LPVOID)NULL);
    SetWindowTextW(hwndLog, bufferWEditPath);
    hwndEditFilename = CreateWindowW(L"edit", reinterpret_cast<LPCWSTR>(bufferWEditFilename), WS_CHILD | WS_VISIBLE | SS_LEFT | WS_BORDER,
                                  PADDING + WINDOW_WIDTH/2, PADDING, WINDOW_WIDTH/2 - 2*10, winH,
                                  hwndMain, (HMENU)1, (HINSTANCE)NULL, (LPVOID)NULL);
    SetWindowTextW(hwndLog, bufferWEditFilename);
    winH = WINDOW_HEIGHT - 3*PADDING - GetSystemMetrics(SM_CYCAPTION);
    hwndLog = CreateWindowW(L"static", reinterpret_cast<LPCWSTR>(bufferWLog), WS_CHILD | WS_VISIBLE | SS_LEFT | WS_BORDER,
                            PADDING, 2*PADDING + LINE_SIZE, WINDOW_WIDTH - 2*10, winH,
                            hwndMain, (HMENU)1, (HINSTANCE)NULL, (LPVOID)NULL);
    SetWindowTextW(hwndLog, bufferWLog);

    RegisterHotKey(hwndMain, 100, MOD_CONTROL | MOD_NOREPEAT, KEY_S);
    RegisterHotKey(hwndMain, 101, MOD_NOREPEAT, KEY_F4);

    // GetMessage is blocking.
    while (GetMessage(&msg, NULL, 0, 0) != 0)
    {
        TranslateMessage(&msg);  // translates keystrokes into characters
        DispatchMessage(&msg);  // this isn't necessary for single window situation
    } 

    return 0;
}
