# WHY IS THIS ON HOLD

So this got pretty far but I can't figure out how to access the address bar of the
_Save_ dialog... I can send a message to it but it doesn't set the address. I can
send a message to the text field with the filename and that works ok... Not sure
how to fix it.

Also it might be an interesting idea to implement this in Nim.

![](thumb.png)

# IDEA

Imagine you work with ZBrush. You click CTRL + S and a window pops up and you can
choose last file from a history. Basically the saving files can be improved.
It's mostly inspired by making a model with multiple subtools in different files.
It's an extension of the idea of ZBrush proxy meshes.

# How to use it?

**F4** turns on the hotkeys for a second. Then you can for example press `q` to
close the application. For more look into the source code (sorry).